import { useState } from "react";
import TaskForm from "./TaskForm";
import EditForm from "./EditForm";
import TaskDetails from "./TaskDetails";

function TodoList() {
  const [tasks, setTasks] = useState([
    {
      "id": 0,
      "task": "Sample Event 2",
      "starts": "2023-12-20T18:30",
      "ends": "2023-12-20T19:30",
      "details": "This will contain a short description of the event and details. Anything of note can be added here.",
      "completed": false
    }
  ]);
  let [nextId, setNextId] = useState(1);

  const handleNewTask = (newValue) => {
    newValue = {
      ...newValue,
      "id": nextId
    }
    setTasks([...tasks, newValue]);
    setNextId(nextId + 1);
  }

  const handleEditTask = (newTask) => {
    const newTaskList = tasks.filter(task => task.id !== newTask.id);
    setTasks([...newTaskList, newTask]);
  }

  const handleDeleteTask = (taskId) => {
    const newTaskList = tasks.filter(task => task.id !== taskId);
    setTasks(newTaskList);
  }

  return (
    <div>
      <h1>
        My Simple Todo App
      </h1>
      <table>
        <thead>
          <tr>
            <th>Task</th>
            <th>Starts</th>
            <th>Ends</th>
            <th>Details</th>
            <th>Completed?</th>
          </tr>
        </thead>
        <tbody>
          {tasks.sort(function(a, b) {
            let dateA = new Date(a.starts);
            let dateB = new Date(b.starts);
            return dateA - dateB;
          }).map(task => {
            const start = new Date(task.starts);
            const end = new Date(task.ends);
            return (
              <>
              <tr key={task.id}>
                <td>{task.task}</td>
                <td>{start.toLocaleString("us")}</td>
                <td>{end.toLocaleString("us")}</td>
                <td>{task.details}</td>
                <td>{task.completed ? "Yes" : "No"}</td>
                <td><TaskDetails task={task}/></td>
                <td>
                  <EditForm task={task} editTask={handleEditTask} deleteTask={handleDeleteTask}/>
                </td>
              </tr>
              </>
            );
          })}
        </tbody>
      </table>
      <TaskForm newTask={handleNewTask} />
    </div>
  );
}

export default TodoList
