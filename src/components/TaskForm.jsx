import { useState } from "react";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';


function TaskForm({newTask}){
    const [open, setOpen] = useState(false);

    const [formValue, setFormValue] = useState({
        "task": "",
        "starts": "",
        "ends": "",
        "details": "",
        "completed": false
    });

    const handleFormChange = (event) => {
        const info = event.target.value;
        const inputName = event.target.name;
        setFormValue({
            ...formValue,
            [inputName]: info
        });
    }

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setFormValue({
            "task": "",
            "starts": "",
            "ends": "",
            "details": "",
            "completed": false
        });
        setOpen(false);
    };

    const saveTask = (event) => {
        const date1 = new Date(formValue.starts);
        const date2 = new Date(formValue.ends);
        if (date1 <= date2){
            newTask(formValue);
            setFormValue({
                "task": "",
                "starts": "",
                "ends": "",
                "details": "",
                "completed": false
            });
            setOpen(false);
        }
    }
    return(
        <>
            <Button variant="outlined" onClick={handleClickOpen}>
                New task...
            </Button>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Create a New Task</DialogTitle>
                <DialogContent>
                    <TextField
                        type="text"
                        id="title"
                        name="task"
                        value={formValue.task}
                        onChange={handleFormChange}
                        label="Title"
                        margin="normal"
                        fullWidth
                        required
                    />
                    <TextField
                        type="datetime-local"
                        id="starts"
                        name="starts"
                        value={formValue.starts}
                        onChange={handleFormChange}
                        label="Select start date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                        required
                    />
                    <TextField
                        type="datetime-local"
                        id="ends"
                        name="ends"
                        value={formValue.ends}
                        onChange={handleFormChange}
                        label="Select end date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                        required
                    />
                    <TextField
                        id="details"
                        name="details"
                        value={formValue.details}
                        onChange={handleFormChange}
                        label="Task details"
                        margin="normal"
                        fullWidth
                        multiline
                    />
                </DialogContent>
                <DialogActions>
                    <Button color="error" onClick={handleClose} type="reset">Cancel</Button>
                    <Button variant="contained" color="success" onClick={saveTask} type="submit">Save</Button>
                </DialogActions>
            </Dialog>
        </>
    );
}

export default TaskForm;
