import { useState } from "react";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';

function EditForm({task, editTask, deleteTask}) {
    const [formValue, setFormValue] = useState(task);
    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleDelete = () => {
        deleteTask(task.id);
        setOpen(false);
    }

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormValue({
            ...formValue,
            [inputName]: value
        });
    }

    const handleSubmit = (event) => {
        const date1 = new Date(formValue.starts);
        const date2 = new Date(formValue.ends);
        if (date1 <= date2) {
            editTask(formValue);
            setOpen(false);
        }
    }

    return (
        <>
            <Button variant="outlined" onClick={handleClickOpen}>
                Edit
            </Button>
            <Button variant="contained" color="error" onClick={handleDelete} type="reset">
                Delete
            </Button>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Edit Task</DialogTitle>
                <DialogContent>
                    <TextField
                        type="text"
                        id="title"
                        name="task"
                        value={formValue.task}
                        onChange={handleFormChange}
                        label="Title"
                        margin="normal"
                        fullWidth
                        required
                    />
                    <TextField
                        type="datetime-local"
                        id="starts"
                        name="starts"
                        value={formValue.starts}
                        onChange={handleFormChange}
                        label="Select start date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                        required
                    />
                    <TextField
                        type="datetime-local"
                        id="ends"
                        name="ends"
                        value={formValue.ends}
                        onChange={handleFormChange}
                        label="Select end date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                        required
                    />
                    <TextField
                        id="details"
                        name="details"
                        value={formValue.details}
                        onChange={handleFormChange}
                        label="Task details"
                        margin="normal"
                        fullWidth
                        multiline
                    />
                    <FormControlLabel
                        control={<Checkbox
                            name="completed"
                            value={formValue.completed}
                            onChange={handleFormChange}
                            defaultChecked={formValue.completed}
                        />}
                        label="Completed"
                    />
                </DialogContent>
                <DialogActions>
                    <Button color="error" onClick={handleClose} type="reset">Cancel</Button>
                    <Button variant="contained" color="success" onClick={handleSubmit} type="submit">Save</Button>
                </DialogActions>
            </Dialog>
        </>
    );
}

export default EditForm;
